# test-user-audit-lambda

Prototype functions for user-audit-lambda functions.

## To run

Docker must be installed.

1. Create a test Atlas cluster
2. Create a `.env` file in the project root with the following parameter:

```dotenv
MONGODB_URI=<your-mongodb-connection-uri>
MONDODB_COLLECTION=atw-user-audit
```

3. Run the following commands:

```shell
$ docker build . -t test-user-audit-lambda
$ docker run -d -p 3000:3000 test-user-audit-lambda --name test-user-audit-lambda 
```

4. To stop the service, run:

```shell
$ docker stop test-user-audit-lambda
```

## Key functions

Functions are stored in `src/user-audit.js`.

### userAudit(userId)

This function checks whether the userId parameter is present in the MongoDB collection.
If the userId is present, it updates the `lastLogin` date with the current date.
If the userId is **not** present, to adds a new document to the collection with the `userId` and `lastLogin` date.

This is the primary function that would be the lambda used for user audit.

### getAllUsers()

Returns all users from the `users` collection as a JSON object.

### DeleteUserById(userId)

Deletes a user from the `users` collection by the `userId` parameter.

## To test as a microservice

Whilst the intention of this project is to hold prototype lambda functions, it can also be tested as a simple microservice.
First, follow the instructions above to start the project using Docker.
Then, the functions can be tested with the following commands:

### user-audit function

```shell
curl -X --location --request POST 'http://localhost:3000/1' 
```

### get-all-users function

```shell
curl -X GET 'http://localhost:3000/'
```

### delete-user function

```shell
curl -X DELETE 'http://localhost:3000/1'
```
