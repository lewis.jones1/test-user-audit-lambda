const { expect } = require('chai');
const { connectToDatabase } = require('../../src/config/database');

process.env.MONGODB_DATABASE = 'user-audit-test';

describe('Connecting to database',  () => {
  it('should return a database object with the correct database name', async () => {
    const client = await connectToDatabase();
    expect(client.namespace).to.equal('user-audit-test');
  });
});
