const { getAllUsers, deleteUserById, userAudit } = require('../../src/services/user-audit');
const { connectToDatabase } = require('../../src/config/database');
const expect = require('chai').expect;
const sinon = require('sinon');

describe('Get all users', () => {
  let collection;

  beforeEach(async () => {
    const db = await connectToDatabase();
    collection = db.collection('users');
    await collection.deleteMany({});
  });

  it('should return an empty array when no users present', async () => {
    const users = await getAllUsers();
    expect(users.length).to.equal(0);
  });

  it('should return a populated array when a single user present', async () => {
    await collection.insertOne({
      userId: 1,
      lastLogin: new Date()
    });

    const users = await getAllUsers();
    expect(users.length).to.equal(1);
  });

  it('should return a populated arrau with multiple users present', async () => {
    await collection.insertOne({
      userId: 1,
      lastLogin: new Date()
    });
  })

  it('should return an array with correct details', async () => {
    await collection.insertOne({
      userId: 1,
      lastLogin: new Date()
    });
    await collection.insertOne({
      userId: 2,
      lastLogin: new Date()
    });

    const users = await getAllUsers();
    expect(users.length).to.equal(2);
    expect(users[0].userId).to.equal(1);
    expect(users[1].userId).to.equal(2);
  });
});

describe('Deleting a single user', () => {
  let collection;

  beforeEach(async () => {
    const db = await connectToDatabase();
    collection = db.collection('users');
    await collection.deleteMany({});
  });

  it('should delete a user when passed a userId present in database', async () => {
    const userId = 1;
    await collection.insertOne({
      userId,
      lastLogin: new Date()
    });

    let users = await getAllUsers();
    expect(users.length).to.equal(1);

    await deleteUserById(userId);

    users = await getAllUsers();
    expect(users.length).to.equal(0);
  });

  it('should delete correct user when passed a userId present in database', async () => {
    const userIdToDelete = 2;

    await collection.insertOne({
      userId: 1,
      lastLogin: new Date()
    });
    await collection.insertOne({
      userId: userIdToDelete,
      lastLogin: new Date()
    });
    await collection.insertOne({
      userId: 3,
      lastLogin: new Date()
    });

    let users = await getAllUsers();
    expect(users.length).to.equal(3);

    await deleteUserById(userIdToDelete);

    users = await getAllUsers();
    expect(users.length).to.equal(2);
    expect(users[0].userId).to.equal(1);
    expect(users[1].userId).to.equal(3);
  });

  it('should not raise an error if attempting to delete a userId not present in database', async () => {
    await deleteUserById(12345);

    const users = await getAllUsers();
    expect(users.length).to.equal(0);
  });
});

describe('User audit function', () => {
  let collection;

  beforeEach(async () => {
    const db = await connectToDatabase();
    collection = db.collection('users');
    await collection.deleteMany({});
  });

  it('should add a new userId to database if not already present in database', async () => {
    let users = await getAllUsers();
    expect(users.length).to.equal(0);

    await userAudit(1);
    users = await getAllUsers();

    expect(users.length).to.equal(1);
    expect(users[0].userId).to.equal(1);
  });

  it('should not add a new object when passed a userId present in database', async () => {
    await collection.insertOne({
      userId: 1,
      lastLogin: new Date()
    });

    await userAudit(1);
    const users = await getAllUsers();

    expect(users.length).to.equal(1);
    expect(users[0].userId).to.equal(1);
  });

  it('should update lastLogin when passed a userId present in database', async () => {
    await collection.insertOne({
      userId: 1,
      lastLogin: new Date('2022-10-10')
    });

    let users = await getAllUsers();
    expect(users[0].lastLogin.toString()).to.equal(new Date('2022-10-10').toString());

    sinon.stub(Date, 'now').returns(new Date('2022-11-23'));

    await userAudit(1);
    users = await getAllUsers();

    expect(users[0].lastLogin.toString()).to.equal(new Date('2022-11-23').toString());
  });
});
