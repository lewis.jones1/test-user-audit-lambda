const MongoClient = require("mongodb").MongoClient;
require('dotenv').config();

async function connectToDatabase() {
  const client = await MongoClient.connect(process.env.MONGODB_URI);
  return client.db(process.env.MONGODB_DATABASE);
}

module.exports = {
  connectToDatabase
}
