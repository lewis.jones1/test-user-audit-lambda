const { connectToDatabase } = require('../config/database');

async function getAllUsers() {
  const db = await connectToDatabase();
  return db.collection("users").find({}).toArray();
}

async function deleteUserById(userId) {
  const db = await connectToDatabase();
  await db.collection("users").deleteOne({ userId });
}

async function userAudit(userId) {
  const db = await connectToDatabase();
  const collection = db.collection("users");
  const query = { userId };
  let status;
  if (await collection.countDocuments(query) > 0) {
    const updatedValues = { $set: { lastLogin: Date.now() } };
    await collection.updateOne(query, updatedValues);
    status = 204;
  } else {
    const object = {
      userId,
      lastLogin: Date.now(),
    }
    await collection.insertOne(object);
    status = 201;
  }
  return status;
}

module.exports = {
  connectToDatabase,
  userAudit,
  getAllUsers,
  deleteUserById
}
