const express = require('express');
const { userAudit, getAllUsers, deleteUserById } = require('./services/user-audit');
const app = express();
const port = 3000;

app.post('/:userId', async (req, res) => {
  const userId = req.params.userId;
  try {
    const status = await userAudit(userId);
    res.status(status);
    res.send();
  } catch (e) {
    console.log(e);
    res.status(500);
    res.send();
  }
});

app.get('/', async (req, res) => {
  try {
    const allUsers = await getAllUsers();
    res.status(200);
    res.send(allUsers);
  } catch (e) {
    console.log(e);
    res.status(500);
    res.send();
  }
});

app.delete('/:userId', async (req, res) => {
  try {
    await deleteUserById(req.params.userId)
    res.status(204);
    res.send();
  } catch (e) {
    console.log(e);
    res.status(500);
    res.send();
  }
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
