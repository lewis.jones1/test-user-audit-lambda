FROM node:18-alpine

WORKDIR /app
COPY src/ ./src
COPY ./package*.json ./
COPY ./.env ./

RUN npm install

EXPOSE 3000

ENTRYPOINT ["node", "src/app.js"]
